FROM resin/rpi-raspbian

MAINTAINER Daniel Sadowski danielraq@gmail.com

RUN apt-get update && \
        apt-get install -y git libudev-dev cmake make gcc g++ libssl-dev git curl libcurl4-openssl-dev libusb-dev wiringpi python3-dev wget

# OPEN-ZWAWE INSTALLATION

RUN git clone --depth 2 https://github.com/OpenZWave/open-zwave.git /root/open-zwave && \
	ln -s /root/open-zwave /root/open-zwave-read-only && \
	cd /root/open-zwave && \
	make && \
	make install


# BOOST LIBRARIES

RUN apt-get remove libboost-dev libboost-thread-dev libboost-system-dev libboost-atomic-dev libboost-regex-dev libboost-date-time1.55-dev libboost-date-time1.55.0 libboost-atomic1.55.0 libboost-regex1.55.0 libboost-iostreams1.55.0 libboost-iostreams1.55.0 libboost-iostreams1.55.0 libboost-serialization1.55-dev libboost-serialization1.55.0 libboost-system1.55-dev libboost-system1.55.0 libboost-thread1.55-dev libboost-thread1.55.0 libboost1.55-dev && \
	apt-get autoremove 

RUN	mkdir boost && \
	cd boost && \
	wget https://sourceforge.net/projects/boost/files/boost/1.62.0/boost_1_62_0.tar.gz/download && \
	tar xvfz download && \
	rm download && \
	cd boost_1_62_0/ && \
	./bootstrap.sh && \
	./b2 stage threading=multi link=static --with-thread --with-date_time --with-system --with-atomic --with-regex && \
	sudo ./b2 install threading=multi link=static --with-thread --with-date_time --with-system --with-atomic --with-regex && \
	cd ../../ && \
	rm -Rf boost/

#DOMOTICZ INSTALLATION

RUN git clone https://github.com/domoticz/domoticz.git /root/domoticz && \
	cd /root/domoticz && \
	cmake -USE_STATIC_OPENZWAVE -DCMAKE_BUILD_TYPE=Release CMakeLists.txt  && \
	make 

#CLEANING
RUN apt-get remove git wget -y  && \
          apt-get clean && \
          apt-get autoclean && \
          rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#START CONFIG

EXPOSE 8080
CMD ["/root/domoticz/domoticz", "-www", "8080"]
