# README #

Docker build setup for Domoticz and OpenZwave. 

### What is this repository for ? ###

Ulatwic sobie zycie

### What you need ? ###
* Rasbian
* Docker
* A lot of time

### What we have inside ? ###

* Base docker image: resin/rpi-raspbian
* OpenZWave: https://github.com/OpenZWave/open-zwave.git
* Domoticz source: https://github.com/domoticz/domoticz.git
* Boost source: https://sourceforge.net/projects/boost/files/boost/1.62.0/boost_1_62_0.tar.gz/download

### Build ###

* cd domoticz_docker
* sudo docker build -t domoticz_openzwave

### Start ###
he basic command is this:

    docker run --device=/dev/ttyUSB0 -v /etc/localtime:/etc/localtime -v /SOMEPATH/domoticz.db:/root/domoticz/domoticz.db:rw -p 8080:8080 --name domoticz --restart=always -d domoticz_openzwave


Explanations

* `docker run -d my_domoticz` : the basic run command
* `-v /etc/localtime:/etc/localtime` : use time of the host 
* `--device=/dev/ttyUSB0` means we expose a device we need to the container.
 * the old way to do this was to use --privileged, but this is a better option
 * when using --privileged, use something like: sudo docker run `---privileged -v /dev/bus/usb:/dev/bus/usb`
 * as you see you might need to play with the device name. ttyUSB0, /dev/bus/usb, /dev/bus/usb/00x/00y .. try using lsusb to find out your device
* `-v /SOMEPATH/domoticz.db:/root/domoticz/domoticz.db` mounts the 'backups' file to the created volume.
* `-p 8080:8080` means that we expose the 8080 port to local 8084
 * domoticz (and our docker install) run on port 8080, but if you have anything running on your machine, this could be an issue